// Bootstrap carousel
$('.carousel').carousel({
  interval: 0
});

// scroll left
$('#slide-left').click(function(event){
    event.preventDefault();
    $('.-slide').animate({scrollLeft:'-=400'},500 , function(){});
});
// scroll right
$('#slide-right').click(function(event){
    event.preventDefault();
    $('.-slide').animate({scrollLeft:'+=400'},500 , function(){});
});

// Owl carousel
$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    items: 4,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:2
        },
        1000:{
            items:4
        }
    },
    navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
  });
});

// Elevate zoom
$('#zoom_01').elevateZoom();
