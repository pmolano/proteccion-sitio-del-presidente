# README #

Este README documentará el objetivo del repositorio, los pasos necesarios para que se configure la aplicación, entre otros.

### ¿Para qué es este repositorio? ###
(Breve resumen sobre contenido del repositorio, funcionalidades, lenguaje, etc.)

### ¿Cómo se configura? ###
(Información necesaria para configurar el ambiente de desarrollo.)

### Proyecto en los que está implementado ###
(Aplica para componentes reutilizables, se debe indicar en cuáles proyectos está implementado.)

### TODO ###
(Cosas pendientes por hacer, que no se alcanzó a implementar.)

### Change log ###
(Ingresar la descripción del cambio realizado en cada versión (especificar la versión).)